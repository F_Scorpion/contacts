package com.scorpion.contact.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.scorpion.contact.database.ContactDatabase;
import com.scorpion.contact.database.beans.Contact;

/**
 * @author Scorpion on 2017-11-08.
 */

public class DeleteContactTask extends AsyncTask<Contact, Exception, Boolean> {
    private ContactDatabase mDB;
    private DeleteContactListener mDeleteContactListener;

    public DeleteContactTask(Context context) {
        mDB = ContactDatabase.getInstance(context);
    }

    @Override
    protected Boolean doInBackground(Contact... contacts) {
        mDB.contactDao().delete(contacts);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        if (mDeleteContactListener == null) return;

        mDeleteContactListener.onSuccess();
    }

    public interface DeleteContactListener {
        void onSuccess();
    }

    public void setDeleteContactListener(DeleteContactListener listener) {
        mDeleteContactListener = listener;
    }
}
