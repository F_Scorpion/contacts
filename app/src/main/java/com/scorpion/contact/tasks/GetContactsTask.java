package com.scorpion.contact.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.scorpion.contact.database.ContactDatabase;
import com.scorpion.contact.database.beans.Contact;

import java.util.List;

/**
 * @author Scorpion on 2017-11-08.
 */

public class GetContactsTask extends AsyncTask<Void, Exception, List<Contact>> {


    private ContactDatabase mDB;
    private GetContactsListener mGetContactsListener;

    public GetContactsTask(Context context) {
        mDB = ContactDatabase.getInstance(context);
    }

    @Override
    protected List<Contact> doInBackground(Void... voids) {
        return mDB.contactDao().getAll();
    }

    @Override
    protected void onPostExecute(List<Contact> contacts) {
        super.onPostExecute(contacts);
        if (mGetContactsListener == null) return;

        mGetContactsListener.onSuccess(contacts);
    }

    public interface GetContactsListener {
        void onSuccess(List<Contact> data);
    }

    public void setGetContactsListener(GetContactsListener listener) {
        mGetContactsListener = listener;
    }
}
