package com.scorpion.contact.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.scorpion.contact.database.ContactDatabase;
import com.scorpion.contact.database.beans.Contact;

/**
 * @author Scorpion on 2017-11-08.
 */

public class AddContactTask extends AsyncTask<Contact, Exception, Boolean> {
    private ContactDatabase mDB;
    private AddContactListener mAddContactListener;

    public AddContactTask(Context context) {
        mDB = ContactDatabase.getInstance(context);
    }

    @Override
    protected Boolean doInBackground(Contact... contacts) {
        mDB.contactDao().insertAll(contacts);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (mAddContactListener == null) return;

        mAddContactListener.onSuccess();
    }

    public interface AddContactListener {
        void onSuccess();
    }

    public void setAddContactListener(AddContactListener listener) {
        mAddContactListener = listener;
    }
}
