package com.scorpion.contact.tasks;

import android.content.Context;
import android.os.AsyncTask;

import com.scorpion.contact.database.ContactDatabase;
import com.scorpion.contact.database.beans.Contact;

/**
 * Created by Scorpion on 2017-11-08.
 */

public class EditContactTask extends AsyncTask<Contact, Exception, Boolean> {

    private ContactDatabase mDB;
    private EditContactListener mEditContactListener;

    public EditContactTask(Context context){
        mDB = ContactDatabase.getInstance(context);
    }
    @Override
    protected Boolean doInBackground(Contact... contacts) {
        mDB.contactDao().update(contacts);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        if (mEditContactListener == null) return;

        mEditContactListener.onSuccess();
    }

    public interface EditContactListener{
        void onSuccess();
    }

    public void setEditContactListener(EditContactListener listener){
        mEditContactListener = listener;
    }
}
