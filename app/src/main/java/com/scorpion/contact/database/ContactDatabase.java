package com.scorpion.contact.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.scorpion.contact.database.beans.Contact;
import com.scorpion.contact.database.beans.ContactDao;

/**
 * @author Scorpion on 2017-11-08.
 */

@Database(entities = {Contact.class}, version = 1)
public abstract class ContactDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "contacts.db";
    private static ContactDatabase mInstance;

    public abstract ContactDao contactDao();

    public static ContactDatabase getInstance(Context context) {
        if (mInstance == null) {
            mInstance = Room.databaseBuilder(context, ContactDatabase.class, DATABASE_NAME).build();
        }

        return mInstance;
    }
}
