package com.scorpion.contact.database.beans;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

/**
 * @author Scorpion on 2017-11-08.
 */

@Dao
public interface ContactDao {

    @Query("SELECT * FROM contacts")
    List<Contact> getAll();

    @Insert
    void insertAll(Contact... contacts);

    @Delete
    void delete(Contact... contacts);

    @Update
    void update(Contact... contacts);

    @Query("SELECT * FROM contacts WHERE name LIKE :name LIMIT 1")
    Contact findByName(String name);
}
