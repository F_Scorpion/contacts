package com.scorpion.contact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.scorpion.contact.adapters.AdapterListContact;
import com.scorpion.contact.addContact.ActivityAddContact;
import com.scorpion.contact.database.beans.Contact;
import com.scorpion.contact.tasks.GetContactsTask;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AdapterListContact mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityAddContact.class);
                startActivity(intent);
            }
        });

        mAdapter = new AdapterListContact(this);

        ListView list = findViewById(R.id.list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Contact contact = mAdapter.getItem(i);
                startActivityEdit(contact);
            }
        });
        list.setAdapter(mAdapter);
    }

    private void startActivityEdit(Contact contact) {
        Intent intent = new Intent(this, ActivityAddContact.class);
        intent.putExtra(ActivityAddContact.CONTACT, contact);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getListContact();
    }

    private void getListContact() {
        GetContactsTask task = new GetContactsTask(this);
        task.setGetContactsListener(new GetContactsTask.GetContactsListener() {
            @Override
            public void onSuccess(List<Contact> data) {
                mAdapter.clear();
                mAdapter.addAll(data);
                mAdapter.notifyDataSetChanged();
            }
        });
        task.execute();
    }
}
