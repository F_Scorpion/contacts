package com.scorpion.contact.addContact;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.scorpion.contact.R;
import com.scorpion.contact.database.beans.Contact;
import com.scorpion.contact.tasks.AddContactTask;
import com.scorpion.contact.tasks.DeleteContactTask;
import com.scorpion.contact.tasks.EditContactTask;

/**
 * @author Scorpion on 2017-11-08.
 */

public class ActivityAddContact extends AppCompatActivity {

    public static final String CONTACT = "contact";
    private EditText mEdtName, mEdtEmail, mEdtPhone;
    private Contact mContact;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_contact);

        mEdtName = findViewById(R.id.edtName);
        mEdtEmail = findViewById(R.id.edtEmail);
        mEdtPhone = findViewById(R.id.edtPhone);

        View btnDelete = findViewById(R.id.btnDelete);

        if (getIntent().hasExtra(CONTACT)) {
            mContact = (Contact) getIntent().getSerializableExtra(CONTACT);
            btnDelete.setVisibility(View.VISIBLE);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    delete();
                }
            });

            mEdtName.setText(mContact.getName());
            mEdtEmail.setText(mContact.getEmail());
            mEdtPhone.setText(mContact.getPhone());
        } else {
            mContact = new Contact();
            btnDelete.setVisibility(View.GONE);
        }

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
    }

    private void delete() {
        DeleteContactTask task = new DeleteContactTask(this);
        task.setDeleteContactListener(new DeleteContactTask.DeleteContactListener() {
            @Override
            public void onSuccess() {
                finish();
            }
        });
        task.execute(mContact);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_contact, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.actionDone) {
            save();
        }
        return super.onOptionsItemSelected(item);
    }

    private void save() {
        mContact.setName(mEdtName.getText().toString().trim());
        mContact.setEmail(mEdtEmail.getText().toString().trim());
        mContact.setPhone(mEdtPhone.getText().toString().trim());

        if (mContact.getId() > 0) {
            edit();
            return;
        }

        add();
    }

    private void edit() {
        EditContactTask task = new EditContactTask(this);
        task.setEditContactListener(new EditContactTask.EditContactListener() {
            @Override
            public void onSuccess() {
                finish();
            }
        });
        task.execute(mContact);
    }

    private void add() {
        AddContactTask task = new AddContactTask(this);
        task.setAddContactListener(new AddContactTask.AddContactListener() {
            @Override
            public void onSuccess() {
                finish();
            }
        });
        task.execute(mContact);
    }
}
