package com.scorpion.contact.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.scorpion.contact.R;
import com.scorpion.contact.database.beans.Contact;

/**
 * @author Scorpion on 2017-11-08.
 */

public class AdapterListContact extends ArrayAdapter<Contact> {


    public AdapterListContact(@NonNull Context context) {
        super(context, 0);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contact_list, null);
            holder = new ViewHolder();
            holder.txvName = convertView.findViewById(R.id.name);
            holder.txvPhone = convertView.findViewById(R.id.phone);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Contact contact = getItem(position);

        holder.txvName.setText(contact.getName());
        holder.txvPhone.setText(contact.getPhone());
        return convertView;
    }

    private class ViewHolder {
        private TextView txvName, txvPhone;
    }
}
